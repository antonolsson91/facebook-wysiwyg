// ==UserScript==

// @version     1.0.0
// @name        Facebook WYSIWYG
// @description Ett steg mot att skapa en WYSIWYG-editor för Facebook-kommentarsfältet men hittils en ruta där man kan hämta ändrad text.
// @namespace   anton-olsson@outlook.com
// @author      Anton

// @resource    main css/main.css

// Libraries:   https://developers.google.com/speed/libraries/
// @require     https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js
// @require     js/functions.js
// @require     js/main.js

// @match       *://www.facebook.com/*

// @grant       GM_getResourceText
// @grant       GM_setClipboard
// @noframes


// @downloadURL https://bitbucket.org/antonolsson91/userscript-template/raw/master/userscript.user.js
// @updateURL   https://bitbucket.org/antonolsson91/userscript-template/raw/master/userscript.user.js

// ==/UserScript==